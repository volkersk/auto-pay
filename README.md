# auto-pay
## Prerequisites
```
sudo apt install python3-pip
pip3 install requests
```


## Installation
Clone the git repo:
```
git clone https://bitbucket.org/volkersk/auto-pay.git
```

Configure *config.json*:

* poollogs_url: Url to the poollogs containing the delegates to_pay
* address_delegate: Used as the sender address for all transactions
* address_payout: Used as the recipient address for the payout transaction
* address_vote: Used as the recipient address for the remaining balance transaction. *Optional*, leave an empty String to disable
* node_url: Location of the node. For example; *http://localhost:10000*, *http://127.0.0.1:10000*, *https://corenode1.oxycoin.io:10001*, *https://wallet.oxycoin.io*, etc
* secret: First passphrase of your wallet. Must match the wallet of the *address_delegate*
* second_secret: Second passphrase of your wallet. *Optional*, leave an empty String to use no second secret


## Example crontab
Edit your crontab with the example below (command: crontab -e)

```
15 8 * * Sat cd ~/auto-pay && python3 ~/auto-pay/autopay.py >> ~/auto-pay/logs/error.log 2>&1
```


## Test mode
Run the script with the flag `-test` to simulate an automatic payment. No actual transactions will be made.
