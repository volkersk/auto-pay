import requests
import json
import sys
import time
import argparse

if sys.version_info[0] < 3:
    print('python2 not supported, please use python3')
    sys.exit()

parser = argparse.ArgumentParser(description='Automatic payment script')
parser.add_argument(
    '-test',
    action='store_true',
    help='Use this flag to run the script in test mode, no transactions will be made'
)
args = parser.parse_args()


def handle_error(ex, msg, exit):
    error_msg = time.strftime('%Y-%m-%d %H:%M:%S: ') + msg
    if ex is not None:
        error_msg += ': ' + str(ex)
    print(error_msg)
    if exit:
        sys.exit()


def send_transaction(node_url, address_to, amount, secret, second_secret):
    if mode_test:
        print('Test transaction of ' + str(float(amount) / 100000000) + ' to ' + address_to)
        return

    try:
        request_url = node_url + '/api/transactions'
        if second_secret:
            body_json = {"amount": amount, "recipientId": address_to, "secret": secret, "secondSecret": second_secret}
        else:
            body_json = {"amount": amount, "recipientId": address_to, "secret": secret}

        response = requests.put(request_url, json=body_json)
        if response.status_code == 200:
            response_json = response.json()
            if not response_json['success']:
                handle_error(None, 'Failed to send transaction', False)
        else:
            handle_error(None, str(response.status_code) + ' Failed to send transaction', False)
    except Exception as ex:
        handle_error(ex, 'Unable to send transaction', False)


def get_to_pay(poollogs_url, address_delegate):
    try:
        response = requests.get(poollogs_url)
        if response.status_code == 200:
            response_json = response.json()
            if not response_json['payout_done']:
                if mode_test:
                    print('Continuing despite payout_done is false due to test mode..')
                else:
                    handle_error(None, 'Aborting, payout_done is false', True)

            delegates = response_json['delegates']
            delegate = delegates[address_delegate]
            if delegate is None:
                to_pay = 0
            else:
                to_pay = int(delegate['to_pay'] * 100000000)

            if to_pay <= 0:
                handle_error(None, 'Aborting, this delegate has nothing to pay', True)

            return to_pay
        else:
            handle_error(None, str(response.status_code) + ' Failed to get amount to pay', True)
    except Exception as ex:
        handle_error(ex, 'Unable to get the amount to pay', True)


def get_node_url():
    node_url = conf['node_url']
    if not node_url.startswith('http'):
        handle_error(None, 'node_url needs to be in the format http://localhost:<port>', True)
    if node_url.endswith('/'):
        node_url = node_url[:-1]

    return node_url


def get_balance(node_url, address_delegate):
    try:
        request_url = node_url + '/api/accounts/getBalance?address=' + address_delegate
        response = requests.get(request_url)
        if response.status_code == 200:
            response_json = response.json()
            if response_json['success']:
                return int(response_json['balance'])
            else:
                handle_error(None, 'Failed to get balance', True)
        else:
            handle_error(None, str(response.status_code) + ' Failed to get balance', True)
    except Exception as ex:
        handle_error(ex, 'Unable to get balance', True)


def auto_pay():
    try:
        node_url = get_node_url()
        address_delegate = conf['address_delegate']
        address_payout = conf['address_payout']
        address_vote = conf['address_vote']
        poollogs_url = conf['poollogs_url']

        secret = conf['secret']
        second_secret = conf['second_secret']

        to_pay = get_to_pay(poollogs_url, address_delegate)

        balance = get_balance(node_url, address_delegate)
        if balance < to_pay:
            handle_error(None, 'Not enough balance to pay ' + str(to_pay), True)

        send_transaction(node_url, address_payout, to_pay, secret, second_secret)

        remainder = balance - to_pay - 100000000
        if remainder > 0 and address_vote is not "":
            send_transaction(node_url, address_vote, remainder, secret, second_secret)
    except Exception as ex:
        handle_error(ex, 'Auto pay failed with generic exception', True)


try:
    mode_test = args.test
    conf = json.load(open('config.json', 'r'))
    auto_pay()
except Exception as ex:
    handle_error(ex, 'Unable to load config file', True)
